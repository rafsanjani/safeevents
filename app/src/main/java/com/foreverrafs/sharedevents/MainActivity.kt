package com.foreverrafs.sharedevents

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doOnTextChanged
import com.foreverrafs.sharedevents.databinding.ActivityMainBinding
import com.foreverrafs.sharedevents.ui.main.SectionsPagerAdapter
import com.foreverrafs.sharedevents.ui.main.SharedViewModel
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val viewModel: SharedViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val sectionsPagerAdapter = SectionsPagerAdapter(this)
        val viewPager = binding.viewPager

        viewPager.adapter = sectionsPagerAdapter

        TabLayoutMediator(binding.tabs, viewPager) { tab, position ->
            when (position) {
                0 -> tab.text = "Home"
                1 -> tab.text = "Other"
            }
        }.attach()

        binding.textInputLayout.editText?.doOnTextChanged { text, _, _, _ ->
            text?.let { viewModel.onQueryChanged(it.toString()) }
        }

    }
}