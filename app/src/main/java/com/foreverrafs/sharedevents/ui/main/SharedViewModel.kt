package com.foreverrafs.sharedevents.ui.main

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.flowOn

class SharedViewModel : ViewModel() {

    private val _query = MutableSharedFlow<String>(replay = 1)

    val query: Flow<String> = _query
        .debounce(200)
        .flowOn(Dispatchers.Main)

    fun onQueryChanged(query: String) {
        _query.tryEmit(query)
    }
}